FROM fedora:latest

COPY . / frontend/
RUN (cd frontend/; npm install;)
CMD (cd frontend/; npm start;)